﻿namespace AISystem.Vines
{
	public enum VinePlatformState
	{
		Idle,
		Spawn,
		WaitForLoop,
		Loop,
		Pierce,
	}
}
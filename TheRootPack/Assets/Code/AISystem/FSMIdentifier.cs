﻿using UnityEngine;

namespace AISystem
{
	[CreateAssetMenu(menuName = "Cuphead/AI/FSMIdentifier", fileName = "FSMIdentifier", order = 0)]
	public class FSMIdentifier : ScriptableObject
	{
	}
}
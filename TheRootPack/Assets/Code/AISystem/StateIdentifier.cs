﻿using UnityEngine;

namespace AISystem
{
	[CreateAssetMenu(menuName = "Cuphead/AI/StateIdentifier", fileName = "StateIdentifier", order = 0)]
	public class StateIdentifier : ScriptableObject
	{
	}
}